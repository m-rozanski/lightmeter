LightMeter
====================

[GooglePlay link][lightmeter]

Simple tool to measure light intensity with your smartphone.

It uses your smartphone's built-in light sensor, so that the result of measurement depends on sensor's accuarcy. 

* Choose between different units
* Calibrate measurement
* See last values on chart
* Save your measurement

---

This is my very first project.
Developing LightMeter taught me basics of Android development, including the process of releasing apps to public on [GooglePlay Store][play] using [GooglePlay Console][playconsole].

---

Technologies used in this project:
====================

* [Google AdMob][admob]
* [AsyncTask][asynctask]
* [SharedPreferences][prefs]
* [Gson][gson]
* [RecyclerView][recycler]
* [MPAndroidChart][chart]
* [Circle-Progress-View][progress]
* Multi Language Support

---

Thanks to [Marcin Czerwiak][czerwiak] for providing me graphics and feedback. See his last app [here][czerwiak-app].


[progress]: https://github.com/jakob-grabner/Circle-Progress-View
[gson]: https://github.com/google/gson
[recycler]: https://developer.android.com/reference/kotlin/androidx/recyclerview/widget/RecyclerView
[play]: https://play.google.com/store/apps
[lightmeter]: https://play.google.com/store/apps/details?id=com.rozanski.app.lightmeter
[admob]: https://admob.google.com/home/
[asynctask]: https://developer.android.com/reference/android/os/AsyncTask
[chart]: https://github.com/PhilJay/MPAndroidChart
[playconsole]: https://developer.android.com/distribute/console
[czerwiak]: https://bitbucket.org/%7B73713c21-26dd-4a6a-9a7b-222856431c04%7D/
[czerwiak-app]: https://play.google.com/store/apps/details?id=io.appsly.drinkwater
[prefs]: https://developer.android.com/reference/android/content/SharedPreferences