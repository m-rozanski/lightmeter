/*
    ListItem class for RecyclerView component in Navigation Drawer
*/
package com.rozanski.app.lightmeter;

import java.util.Calendar;

public class ListItem {
    private String name = "Default";
    private double actual;
    private double minimum;
    private double maximum;
    private long average;
    private double averageD;
    private double factor;
    private boolean isLux;
    private Calendar calendar;


    public ListItem(){
        actual = 0;
        minimum = 0;
        maximum = 0;
        average = 0;
        factor = 0;
        isLux = true;
    }

    public ListItem(double actual, double minimum, double maximum, long average, double averageD,
                    double factor, boolean isLux, Calendar calendar) {
        this.actual = actual;
        this.minimum = minimum;
        this.maximum = maximum;
        this.average = average;
        this.averageD = averageD;
        this.factor = factor;
        this.isLux = isLux;
        this.calendar = calendar;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public double getActual() {
        return actual;
    }

    public double getMinimum() {
        return minimum;
    }

    public double getMaximum() {
        return maximum;
    }

    public long getAverage() {
        return average;
    }

    public double getAverageD() {
        return averageD;
    }

    public double getFactor() {
        return factor;
    }

    public boolean isLux() {
        return isLux;
    }

    public Calendar getDate() {return calendar;}
}
