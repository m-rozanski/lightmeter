package com.rozanski.app.lightmeter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewTreeObserver;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.SeekBar;
import android.widget.TextView;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import at.grabner.circleprogress.CircleProgressView;
import java.lang.reflect.Type;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import android.os.AsyncTask;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, SensorEventListener, PopupMenu.OnMenuItemClickListener{

    //basic values
    private SensorManager mSensorManager;
    private Sensor mLight;
    double eventValues = 0;
    double actual = 0;
    double minimum = 0;
    double maximum = 0;
    double averageD = 0;
    TextView result;
    TextView min_txt;
    TextView max_txt;
    TextView avg_txt;
    TextView units_txt;
    static public boolean isLux = true;
    static public final double convert_factor = 0.092903;   //lux to fc
    double factor = 1.0;                //calibration
    boolean start = false;              //to measure or not
    boolean first = true;               //check if first measure has been done
    long average_counter = 0;           //a variable to calculate average
    AsyncTask asyncTask;
    SharedPreferences sharedPref;
    SharedPreferences.Editor sharedPrefEditor;

    ImageView startButton;
    ImageView circle;                   //just image of circle
    CircleProgressView circleBar;       //progress bar to show value of measurement

    //chart values
    LineChart chart;
    List<Double> values = new ArrayList<>();
    List<Entry>  entryList = new ArrayList<>();
    LineData chartData;
    LineDataSet chartDataSet;
    List<ILineDataSet> chartListDataSets = new ArrayList<ILineDataSet>();
    int count = 0;

    //values formatter
    DecimalFormat format = new DecimalFormat("0");
    DecimalFormat format_factor = new DecimalFormat("0.00");

    //dialog_calibration
    View dialogView;
    TextView dialogCurrentValueTxt;
    boolean acceptNewVal = false;
    boolean hasWorking = false;
    boolean reset = false;

    // Navigation Drawer
    private RecyclerView mRecyclerView;
    private ListAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    ArrayList<String> myDataset;
    ArrayList<ListItem> dataset = new ArrayList<>();
    SimpleDateFormat simpleDateFormat;

    //ads
    private AdView mAdView;
    private String ADS_KEY = "ca-app-pub-5061625846024446~7716619670";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
/*
        View mDecorView = getWindow().getDecorView();
        mDecorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                        | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                        | View.SYSTEM_UI_FLAG_IMMERSIVE);
*/
        setContentView(R.layout.activity_main);

        // load ads
        if(isNetworkConnected()) {
            MobileAds.initialize(this, ADS_KEY);
            //mAdView = findViewById(R.id.adView);
            AdRequest adRequest = new AdRequest.Builder().build();
            mAdView.loadAd(adRequest);
            //mAdView.setVisibility(View.GONE);
        }

        // initialize
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mLight = mSensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
        units_txt = findViewById(R.id.textView12);
        result = findViewById(R.id.textView3);
        min_txt = findViewById(R.id.textView8);
        max_txt = findViewById(R.id.textView9);
        avg_txt = findViewById(R.id.textView10);
        startButton = findViewById(R.id.imageView2);
        circleBar = findViewById(R.id.circleView);
        circle = findViewById(R.id.circle);
        chart = findViewById(R.id.chart);

        // read Shared Preferences or create if not exists ****************************************
        sharedPref = this.getPreferences(Context.MODE_PRIVATE);
        if(sharedPref.getAll().isEmpty()) {
            isLux = true;
            factor = 1;
            sharedPrefEditor = sharedPref.edit();
            sharedPrefEditor.putBoolean("PREFERENCES_IS_LUX", isLux);
            sharedPrefEditor.putInt("PREFERENCES_FACTOR", (int)(factor*100) );
            sharedPrefEditor.commit();
        }
        else {
            isLux = sharedPref.getBoolean("PREFERENCES_IS_LUX", true);
            factor = ((double)(sharedPref.getInt("PREFERENCES_FACTOR", 100)))/100;
            loadListData();

            if(isLux) {
                format = new DecimalFormat("0");
                units_txt.setText(R.string.lux);
            }
            else {
                format = new DecimalFormat("0.00");
                units_txt.setText(R.string.fc);
            }
        }
        //****************************************************************************************

        avg_txt.setText(format.format(averageD));
        min_txt.setText(format.format(minimum));
        max_txt.setText(format.format(maximum));

        circleBar.setValue(0);

        // set size of elements *******************************************************************
        final TextView text = findViewById(R.id.textView2);
        final ImageView image = findViewById(R.id.imageView6);
        final ViewTreeObserver observer = circleBar.getViewTreeObserver();
        observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                text.setText("" + image.getHeight());
                float imgHeight = image.getHeight();

                circleBar.getLayoutParams().height = (int)(imgHeight*0.9);
                circleBar.getLayoutParams().width = (int)(imgHeight*0.9);
                circleBar.setRimWidth((int)(imgHeight*0.04));
                circleBar.setBarWidth((int)(imgHeight*0.05));

                startButton.getLayoutParams().height = (int)(imgHeight*0.29);
                startButton.getLayoutParams().width = (int)(imgHeight*0.29);

                circle.getLayoutParams().height = (int)(imgHeight*0.37);
                circle.getLayoutParams().width = (int)(imgHeight*0.37);

                result.getLayoutParams().height = (int)(circle.getHeight()*0.45);
                result.getLayoutParams().width = (int)(circle.getHeight()*0.7);

                if(observer.isAlive());
                circleBar.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });
        //****************************************************************************************

        drawEmptyChart();

        dialogView = getLayoutInflater().inflate(R.layout.dialog_calibration, null);
        dialogCurrentValueTxt = dialogView.findViewById(R.id.current_val);

        // NAVIGATION DRAWER **********************************************************************

        //NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        //navigationView.setNavigationItemSelectedListener(this);

        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        myDataset = new ArrayList<String>();

        mAdapter = new ListAdapter(dataset);
        mAdapter.setOnItemClickListener(new ListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                showItemDialog(position);
            }
        });
        mRecyclerView.setAdapter(mAdapter);

        Button button = findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addNewListItem();
            }
        });

        simpleDateFormat = new SimpleDateFormat("dd MMM yyyy HH:mm:ss");

        //****************************************************************************************

        // show alert if light sensor is not available
        if(mLight == null) {
            circleBar.setRimColor(Color.RED);
            result.setText("ERROR");
            result.setTextColor(Color.RED);
            min_txt.setTextColor(Color.RED);
            max_txt.setTextColor(Color.RED);
            avg_txt.setTextColor(Color.RED);
            result.setClickable(false);
            startButton.setVisibility(View.INVISIBLE);
            result.setVisibility(View.VISIBLE);
            showLightSensorAlertDialog();
            button.setClickable(false);
            button.setText("ERROR");
            button.setTextColor(getResources().getColor(R.color.DarkRed));
        }
    }

    private boolean isNetworkConnected() {
        mAdView = (AdView) findViewById(R.id.adView);
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        if (ni == null) {
            // There are no active networks.
            //Set the visibility to "gone".
            //You can set visibility to gone here or when the function returns,
            //that is why there is a return false and true.
            mAdView.setVisibility(View.GONE);
            return false;
        } else
            return true;
    }

    private void saveListData(){
        sharedPrefEditor = sharedPref.edit();
        Gson gson = new Gson();
        String json = gson.toJson(dataset);
        sharedPrefEditor.putString("item_list", json);
        sharedPrefEditor.commit();
    }

    private void loadListData(){
        Gson gson = new Gson();
        String json = sharedPref.getString("item_list", null);
        if(json != null) {
            Type type = new TypeToken<ArrayList<ListItem>>() {
            }.getType();
            dataset = gson.fromJson(json, type);
        }
    }

    public void addNewListItem(){
        Calendar calendar = Calendar.getInstance();
        double actual = 0;
        if(!first){
         actual = Double.valueOf(result.getText().toString().replaceAll(",", "."));
        }
        dataset.add(new ListItem(actual, minimum, maximum, 0, averageD, factor, isLux, calendar));
        dataset.get(dataset.size()-1).setName(getResources().getString(R.string.newItem));
        mAdapter.notifyItemInserted(dataset.size()-1);
        mRecyclerView.scrollToPosition(mAdapter.getItemCount() - 1);
    }

    public void showItemDialog(final int position){
        if(start) {
            hasWorking = true;
            stop();             //stop measurement for reading
        }
        else
            hasWorking = false;

        final ListItem item = dataset.get(position);

        AlertDialog.Builder mBuilder = new AlertDialog.Builder(MainActivity.this);
        View dialogView = getLayoutInflater().inflate(R.layout.dialog_list_item, null);
        mBuilder.setView(dialogView);
        final AlertDialog dialog = mBuilder.create();
        final TextView ok_btn = dialogView.findViewById(R.id.ok_action);
        TextView delete_btn = dialogView.findViewById(R.id.delete_action);
        TextView editButton = dialogView.findViewById(R.id.edit_action);
        final EditText title = dialogView.findViewById(R.id.title);
        TextView value = dialogView.findViewById(R.id.value);
        TextView min = dialogView.findViewById(R.id.minimum);
        TextView max = dialogView.findViewById(R.id.maximum);
        TextView avg = dialogView.findViewById(R.id.average);
        TextView factor = dialogView.findViewById(R.id.factor);
        TextView date = dialogView.findViewById(R.id.date);

        title.setEnabled(false);
        title.setImeOptions(EditorInfo.IME_ACTION_DONE);
        title.setRawInputType(InputType.TYPE_CLASS_TEXT  | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);

        if(isLux){
            if(item.isLux()){
                value.setText(getResources().getString(R.string.Value) + format.format(item.getActual()));
                min.setText(getResources().getString(R.string.Minimum) + format.format(item.getMinimum()));
                max.setText(getResources().getString(R.string.Maximum) + format.format(item.getMaximum()));
                avg.setText(getResources().getString(R.string.Average) + format.format(item.getAverageD()));
            }
            else{
                value.setText(getResources().getString(R.string.Value) + format.format((item.getActual() / convert_factor)));
                min.setText(getResources().getString(R.string.Minimum) + format.format(item.getMinimum() / convert_factor));
                max.setText(getResources().getString(R.string.Maximum) + format.format(item.getMaximum() / convert_factor));
                avg.setText(getResources().getString(R.string.Average) + format.format(item.getAverageD() / convert_factor));
            }
        }
        else{
            if(item.isLux()){
                value.setText(getResources().getString(R.string.Value) + format.format((item.getActual() * convert_factor)));
                min.setText(getResources().getString(R.string.Minimum) + format.format(item.getMinimum() * convert_factor));
                max.setText(getResources().getString(R.string.Maximum) + format.format(item.getMaximum() * convert_factor));
                avg.setText(getResources().getString(R.string.Average) + format.format(item.getAverageD() * convert_factor));
            }
            else{
                value.setText(getResources().getString(R.string.Value) + format.format(item.getActual()));
                min.setText(getResources().getString(R.string.Minimum) + format.format(item.getMinimum()));
                max.setText(getResources().getString(R.string.Maximum) + format.format(item.getMaximum()));
                avg.setText(getResources().getString(R.string.Average) + format.format(item.getAverageD()));
            }
        }

        if(isLux){
            format = new DecimalFormat("0.00");
            factor.setText(getResources().getString(R.string.Original_factor) + format.format(item.getFactor()));
            format = new DecimalFormat("0");
        }
        else{
            factor.setText(getResources().getString(R.string.Original_factor) + format.format(item.getFactor()));
        }

        date.setText(getResources().getString(R.string.Date) + simpleDateFormat.format(item.getDate().getTime()));

        // do on keyboard DONE action
        title.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    if(title.getText().toString().length() > 0) {
                        title.setEnabled(false);
                        item.setName(title.getText().toString());
                        mAdapter.notifyItemChanged(position);
                        return true;
                    }
                }
                return false;
            }
        });

        // to check if EditText is empty
        title.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(title.getText().toString().length() < 1){
                    dialog.setCancelable(false);
                    ok_btn.setClickable(false);
                    ok_btn.setTextColor(Color.DKGRAY);
                }
                else if(!ok_btn.isClickable()){
                    dialog.setCancelable(true);
                    ok_btn.setClickable(true);
                    ok_btn.setTextColor(Color.WHITE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });

        ok_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                item.setName(title.getText().toString());
                mAdapter.notifyItemChanged(position);
                dialog.dismiss();
            }
        });

        delete_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dataset.remove(position);
                mAdapter.notifyItemRemoved(position);
                dialog.dismiss();
            }
        });

        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                title.setEnabled(true);
                title.setSelection(title.getText().length());
                title.requestFocus();
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(title, InputMethodManager.SHOW_IMPLICIT);
            }
        });

        title.setText(item.getName());

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (hasWorking) stop();   //continue measurement or not
            }
        });

        dialog.show();
    }

    private void drawEmptyChart(){
        chart.getDescription().setEnabled(false);
        chart.getLegend().setEnabled(false);
        chartData = new LineData();
        chart.setData(chartData);
        chart.setClickable(false);
        chart.setPinchZoom(false);
        chart.setDoubleTapToZoomEnabled(false);

        XAxis xAxis = chart.getXAxis();
        xAxis.setGranularity(1f);       // minimum axis-step (interval)
        xAxis.setAxisMinimum(0);
        xAxis.setAxisMaximum(4);
        xAxis.setTextColor(Color.WHITE);

        YAxis leftAxis = chart.getAxisLeft();
        leftAxis.setAxisMinimum(0);
        leftAxis.setAxisMaximum(100);
        leftAxis.setGranularity(1);
        leftAxis.setDrawAxisLine(false);
        leftAxis.setTextColor(Color.WHITE);

        chart.getAxisRight().setEnabled(false);
        chart.invalidate();             //refresh
    }

    public void stop(View v){
        stop();         //just to handle a click
    }

    public void stop() {
        if(start) {
            start = false;
            asyncTask.cancel(true);
            result.setTextColor(Color.GRAY);
        }
        else {
            start = true;
            asyncTask = new ChartAndAverageUpdatingTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            result.setTextColor(Color.WHITE);
        }
    }

    public void showPopup(View v) {
        PopupMenu popup = new PopupMenu(this, v);
        popup.setOnMenuItemClickListener(this);
        popup.inflate(R.menu.popup_menu);
        popup.show();
    }

    public boolean onMenuItemClick(MenuItem item){
        switch (item.getItemId()) {
            case R.id.reset:
                resetMeasurement();
                return true;
            case R.id.changeUnit:
                changeUnits();
                return true;
            case R.id.calibrate:
                calibrate();
                return true;
            //case R.id.about:
             //   showAboutDialog();
              //  return true;
            default:
                return false;
        }
    }

    private void resetMeasurement(){
        actual = 0;
        minimum = 0;
        maximum = 0;
        averageD = 0;
        average_counter = 0;

        if(mLight != null) {
            result.setTextColor(Color.WHITE);
            result.setVisibility(View.INVISIBLE);
            circle.setVisibility(View.INVISIBLE);
            startButton.setVisibility(View.VISIBLE);
            updateBasicValues();
        }
        start = false;
        first = true;

        if(asyncTask != null)
            asyncTask.cancel(true);

        updateAverage();
        drawEmptyChart();
        values.clear();
        count = 0;

        if(mLight != null){
        Toast.makeText(MainActivity.this, getResources().getString(R.string.newMeasurement),
                Toast.LENGTH_LONG).show();
        }
        else {
            Toast.makeText(MainActivity.this, getResources().getString(R.string.sensorError),
                    Toast.LENGTH_LONG).show();
        }
    }

    private void showAboutDialog(){
        if(start) {
            hasWorking = true;
            stop();             //stop measurement for "about" reading
        }
        else
            hasWorking = false;

        AlertDialog.Builder mBuilder = new AlertDialog.Builder(MainActivity.this);
        View dialogView = getLayoutInflater().inflate(R.layout.dialog_about, null);
        mBuilder.setView(dialogView);
        final AlertDialog dialog = mBuilder.create();
        TextView ok_btn = dialogView.findViewById(R.id.ok_action);
        ok_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (hasWorking) stop();   //continue measurement or not
            }
        });

        dialog.show();
    }

    private void showLightSensorAlertDialog(){
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(MainActivity.this);
        View dialogView = getLayoutInflater().inflate(R.layout.dialog_alert_light_sensor, null);
        mBuilder.setView(dialogView);
        final AlertDialog dialog = mBuilder.create();
        dialog.setCancelable(false);

        TextView quit_btn = dialogView.findViewById(R.id.quit_action);
        final TextView ok_btn = dialogView.findViewById(R.id.ok_action);

        quit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //close app
                finishAffinity();
                android.os.Process.killProcess(android.os.Process.myPid());
                System.exit(0);
            }
        });

        //ok button click timer ********************************************************************
        final int seconds = 5;
        ok_btn.setClickable(false);
        ok_btn.setText("Ok [" + seconds + "]");

        class TextUpdatingTask extends AsyncTask<Void, Integer, Void> {
            int i = seconds;
            @Override
            protected Void doInBackground(Void... arg0) {
                try{
                    for(; i >= 0 ; i--){
                        Thread.sleep(1000);
                        publishProgress(i);
                    }
                }
                catch(InterruptedException e){ }
                return null;
            }

            @Override
            protected void onProgressUpdate(Integer... values) {
                super.onProgressUpdate(values);
                switch(values[0]){
                    case 0:
                        ok_btn.setText("OK");
                        ok_btn.setClickable(true);
                        ok_btn.setTextColor(getResources().getColor(R.color.colorAccent));
                        ok_btn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        });
                        break;
                    default:
                        ok_btn.setText("OK [" + values[0] + "]");
                        break;
                }
            }
        }
        //******************************************************************************************

        dialog.show();
        new TextUpdatingTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void changeUnits() {
        double actual = Double.valueOf(result.getText().toString().replaceAll(",", "."));

        if (isLux) {
            isLux = false;
            units_txt.setText(R.string.fc);
            format = new DecimalFormat("0.00");
            actual *= convert_factor;
            minimum *= convert_factor;
            maximum *= convert_factor;
            averageD *= convert_factor;

            int size = values.size();
            for (int i = 0; i < size; i++)
                values.add(values.get(i) * convert_factor);

            for (int i = 0; i < size; i++)
                values.remove(0);

            chart.getAxisLeft().setGranularity(0.5f);
        } else {
            isLux = true;
            units_txt.setText(R.string.lux);
            format = new DecimalFormat("0");
            actual /= convert_factor;
            minimum /= convert_factor;
            maximum /= convert_factor;
            averageD /= convert_factor;

            int size = values.size();
            for (int i = 0; i < size; i++)
                values.add(values.get(i) / convert_factor);

            for (int i = 0; i < size; i++)
                values.remove(0);

            chart.getAxisLeft().setGranularity(1f);
        }

        avg_txt.setText(format.format(averageD));
        min_txt.setText(format.format(minimum));
        max_txt.setText(format.format(maximum));

        //check light sensor
        if (mLight != null) {
            result.setText(format.format(actual));
        } else {
            result.setText("ERROR");
        }

        if (!first) {
            if (maximum > 100)
                circleBar.setValue((int) ((actual * 100) / maximum));
            else
                circleBar.setValue((int) actual);

            updateChart(false);
        }

        mAdapter.notifyDataSetChanged();
    }

    private void showLightSensorAlertToast(){
        Toast.makeText(MainActivity.this, getResources().getString(R.string.sensorError),
                Toast.LENGTH_LONG).show();
    }

    private void calibrate(){
        if(mLight != null) {        //check light sensor first
            if (start) {
                hasWorking = true;
                stop();             //stop measurement for calibration
            } else
                hasWorking = false;

            acceptNewVal = false;   //new factor will be accepted only if user click "set"
            AlertDialog.Builder mBuilder = new AlertDialog.Builder(MainActivity.this);
            View dialogView = getLayoutInflater().inflate(R.layout.dialog_calibration, null);
            dialogCurrentValueTxt = dialogView.findViewById(R.id.current_val);
            dialogCurrentValueTxt.setText(getResources().getString(R.string.currentValue) + " " + format.format(actual));
            final TextView factor_txt = dialogView.findViewById(R.id.factor_txt);
            final SeekBar seekBar = dialogView.findViewById(R.id.seekBar);
            factor_txt.setText(getResources().getString(R.string.currentFactor) + " " + format_factor.format(factor));
            seekBar.setProgress((int) (factor * 100));
            final double factor_copy = factor;
            reset = false;

            seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    if(reset)
                        factor = ((double) progress / 100);
                    else
                        factor = ((double) progress / 100) + 0.01;

                    factor_txt.setText(getResources().getString(R.string.currentFactor) + " " + format_factor.format(factor));

                    if(isLux)
                        dialogCurrentValueTxt.setText(getResources().getString(R.string.currentValue) + " " + format.format(eventValues*factor));
                    else
                        dialogCurrentValueTxt.setText(getResources().getString(R.string.currentValue) + " " + format.format(eventValues*convert_factor*factor));

                    reset = false;
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {
                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                }

            });

            mBuilder.setView(dialogView);

            final AlertDialog dialog = mBuilder.create();

            TextView reset_btn = dialogView.findViewById(R.id.reset_action);
            TextView cancel_btn = dialogView.findViewById(R.id.cancel_action);
            TextView set_btn = dialogView.findViewById(R.id.set_action);

            reset_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    reset = true;
                    seekBar.setProgress(100);
                }
            });

            cancel_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                     acceptNewVal = false;
                    dialog.dismiss();
                }
            });

            set_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    acceptNewVal = true;
                    dialog.dismiss();
                }
            });

            dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    //accept new factor or back to previous
                    if (acceptNewVal) {
                        double actual = Double.valueOf(result.getText().toString().replaceAll(",", "."));

                        actual *= factor / factor_copy;
                        minimum *= factor / factor_copy;
                        maximum *= factor / factor_copy;
                        averageD *= factor / factor_copy;

                        int size = values.size();
                        for (int i = 0; i < size; i++)
                            values.add(values.get(i) * factor / factor_copy);

                        for (int i = 0; i < size; i++)
                            values.remove(0);

                        avg_txt.setText(format.format(averageD));
                        min_txt.setText(format.format(minimum));
                        max_txt.setText(format.format(maximum));
                        result.setText(format.format(actual));

                        if (!first) {
                            if (maximum > 100) {
                                circleBar.setValue((int) ((actual * 100) / maximum));
                            } else {
                                circleBar.setValue((int) actual);
                            }

                            updateChart(false);
                        }

                    } else {
                        factor = factor_copy;
                    }

                    if (hasWorking) stop();   //continue measurement or not
                }
            });

            dialog.show();
        }
        else{
            showLightSensorAlertToast();
        }
    }

    private void updateChart(boolean addNextValue){
        if(addNextValue){
            values.add(actual);

            if (10 < values.size()) {
                count++;
                values.remove(0);
            }
        }

        if(10 <= values.size()){
            chart.getXAxis().resetAxisMaximum();
            chart.getXAxis().resetAxisMinimum();
        }

        entryList.clear();
        for(int i = 0 ; i<values.size() ; i++)
            entryList.add(new Entry(((float)count+i)/2, (values.get(i)).floatValue()));

        chartDataSet = new LineDataSet(entryList, "");
        chartListDataSets.clear();
        chartListDataSets.add(chartDataSet);
        chartData = new LineData(chartListDataSets);
        chart.setData(chartData);

        chart.getAxisLeft().setAxisMaximum((int)maximum+1);

        chartData.setDrawValues(false);
        chartDataSet.setColor(Color.WHITE);
        chartDataSet.setCircleColor(Color.WHITE);

        chart.notifyDataSetChanged();
        chart.invalidate();
    }

    @Override
    public final void onSensorChanged(SensorEvent event) {
        eventValues = event.values[0];

        if(isLux)
            actual = (eventValues*factor);
        else
            actual = eventValues*convert_factor*factor;

        dialogCurrentValueTxt.setText(getResources().getString(R.string.currentValue) + " " + format.format(actual));

        if(start) {
            updateBasicValues();
        }
    }

    public void updateBasicValues(){
        result.setText(format.format(actual));

        if (maximum < actual) maximum = actual;
        if (actual < minimum) minimum = actual;

        max_txt.setText(format.format(maximum));
        min_txt.setText(format.format(minimum));

        if(maximum>100)
            circleBar.setValue((int) ((actual * 100) / maximum));
        else
            circleBar.setValue((int)actual);
    }

    public void updateAverage(){
        average_counter++;
        averageD = ((averageD*(average_counter-1)+actual))/(double)average_counter;

        avg_txt.setText(format.format(averageD));
    }

    @Override
    public final void onAccuracyChanged(Sensor sensor, int accuracy) {
        // Do something here if sensor accuracy changes.
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        // Register a listener for the sensor.
        super.onResume();
        mSensorManager.registerListener(this, mLight, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause() {
        // Be sure to unregister the sensor when the activity pauses.
        super.onPause();

        //save Shared Preferences
        mSensorManager.unregisterListener(this);
        sharedPrefEditor = sharedPref.edit();
        sharedPrefEditor.putBoolean("PREFERENCES_IS_LUX", isLux);
        sharedPrefEditor.putInt("PREFERENCES_FACTOR", (int)(factor*100) );
        sharedPrefEditor.commit();
        saveListData();
    }


    public void start(View v){
        result.setText(format.format(actual));
        startButton.setVisibility(View.INVISIBLE);
        result.setVisibility(View.VISIBLE);
        circle.setVisibility(View.VISIBLE);
        minimum = 99999999;
        start = true;
        first = false;

        updateBasicValues();
        updateAverage();

        //new ChartAndAverageUpdatingTask().execute();
        asyncTask = new ChartAndAverageUpdatingTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        Toast.makeText(MainActivity.this, getResources().getString(R.string.tapAgainToPause),
                Toast.LENGTH_LONG).show();
    }

    public void openDrawer(View v){
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.openDrawer(GravityCompat.START);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }



    // *******************************************************************************************



    public class ChartAndAverageUpdatingTask extends AsyncTask<Void, Void, Void> {
        long systemTime = 0;

        @Override
        protected void onPreExecute() { }

        @Override
        protected Void doInBackground(Void... arg0) {
            //loop which updates chart and average value every 0,5 sec
            while(!isCancelled()) {
                if(System.currentTimeMillis() - systemTime > 500 ) {
                    publishProgress();
                    systemTime = System.currentTimeMillis();
                }
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
            updateChart(true);
            updateAverage();
        }

        @Override
        protected void onPostExecute(Void result) { }

    }

}


