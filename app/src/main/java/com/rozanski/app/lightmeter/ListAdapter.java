/*
    Create the basic adapter extending from RecyclerView.Adapter
*/

package com.rozanski.app.lightmeter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.text.DecimalFormat;
import java.util.ArrayList;

public class ListAdapter extends
        RecyclerView.Adapter<ListAdapter.ViewHolder> {

    private ArrayList<ListItem> items;

    public ListAdapter(ArrayList<ListItem> items) {
        this.items = items;
    }

    @Override
    public ListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View itemView = inflater.inflate(R.layout.list_layout, parent, false);

        ViewHolder viewHolder = new ViewHolder(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ListAdapter.ViewHolder viewHolder, int position) {
        ListItem item = items.get(position);
        DecimalFormat format = new DecimalFormat("0");

        TextView textView = viewHolder.nameTextView;
        textView.setText(item.getName());

        TextView value = viewHolder.valueTextView;

        if(MainActivity.isLux){
            if(item.isLux()){
                value.setText(format.format(item.getActual()));
            }
            else{
                value.setText(format.format(item.getActual() / MainActivity.convert_factor));
            }
        }
        else{
            format = new DecimalFormat("0.00");
            if(item.isLux()){
                value.setText(format.format(item.getActual() * MainActivity.convert_factor));
            }
            else{
                value.setText(format.format(item.getActual()));
            }
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    private OnItemClickListener listener;
    public interface OnItemClickListener {
        void onItemClick(View itemView, int position);
    }
    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView nameTextView;
        public TextView valueTextView;

        public ViewHolder(final View itemView) {
            super(itemView);
            nameTextView = (TextView) itemView.findViewById(R.id.item_name);
            valueTextView = (TextView) itemView.findViewById(R.id.item_value);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onItemClick(itemView, position);
                        }
                    }
                }
            });
        }
    }
}